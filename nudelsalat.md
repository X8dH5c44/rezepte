# Nudelsalat
## Zutaten:
### Salat:
- 280g Nudeln
- 1 Paprika
- 1 mittelgroße rote Zwiebel
- 4 Tomaten
- ca. 200g Gurke
- 150g Essiggurken/Oliven/Mais
### Dressing:
- 1 Dose weiße Bohnen (Abtropfgewicht ca. 260g)
- 160ml Hafer-/Sojamilch
- 40g eingeweichte Cashewkerne
- 2 gepresste Knoblauchzehen
- 1 EL Senf
- 1 EL Apfelessig
- 1/2 TL Salz
- frischer Pfeffer nach Geschmack
## Zubereitung:
### Salat:
- Nudeln kochen, abgießen, abkühlen lassen
- restliche Zutaten kleinschneiden
- alles mit Dressing mischen und eventuell noch mit Salz/Pfeffer/Senf abschmecken
### Dressing:
- alle Zutaten in einen Mixer geben und auf höchster Stufe pührieren,
  bis das Dressing vollständig glatt und cremig ist
