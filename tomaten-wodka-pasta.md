# Tomaten-Wodka-Pasta
## Menge:
2 Portionen
## Zutaten:
- 250g Penne
- 1-2 EL Olivenöl (oder das Öl der getrockneten Tomaten)
- 1 Zwiebel
- 2 Knoblauchzehen, gehackt
- 60ml Wodka (optional Wein)
- 400g geschälte Tomaten (aus der Dose)
- 50g getrocknete Tomaten in Öl eingelegt
- 1 EL Tomatenmark (optional)
- 75g Cashewnüsse (3 Stunden eingeweicht oder für 10 Minuten gekocht)
- 1 TL Oregano
- Salz und Pfeffer
- Chili- und/oder Paprikapulver
- 80ml vom Nudelkochwasser
## Zubereitung:
- Nudeln in einem großen Topf in Salzwasser kochen. Anschließend abgießen und 80ml vom Kochwasser für die Sauce auffangen. 
- In der Zwischenzeit Öl in einer Pfanne erhitzen. Zwiebeln hinzufügen und glasig dünsten. Knoblauch dazugeben und weitere Minuten rösten. 
- Two shots of Wodka eingießen und bei starker Hitze kochen bis der größte Teil der Flüssigkeit verdunstet ist.
- Dann zusammen mit den geschälten Tomaten, getrockneten Tomaten, Cashewnüsse und Oregano in einen Mixer geben und cremig mixen.
-Die Sauce zurück in die Pfanne gießen. Das aufgefangene Nudelkochwasser hinzufügen und bis zur gewünschten Konsistenz köcheln lassen. Mit Salz, Pfeffer und Chili-/Paprikapulver abschmecken.
- Nudeln hinzugeben und vermengen.
