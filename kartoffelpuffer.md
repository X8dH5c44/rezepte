# Vegane Kartoffelpuffer
## Menge:

Für 4 Personen bzw. 20 - 24 Stück


## Zutaten:

- 1kg Kartoffeln, mehligkochend
- 1 kleine Möhre
- 1TL Haferflocken
- 40g Kartoffelstärke
- 1TL Salz


### optional:

- 1TL Pfeffer/Muskat/Kräuter


## Zubereitung:

- Kartoffeln und Möhre waschen, schälen, grob raspeln und 15 Minuten ruhen lassen
- Kartoffelmasse mit Haferflocken, Kartoffelstärke, Salz,
  Pfeffer/Muskat/Kräutern vermengen
- Fett in Pfanne erhitzen,
  pro Puffer 1-2 EL Teig in Pfanne geben und flach drücken;
  von beiden Seiten goldbraun braten
