# Süßkartoffel Kumpir mit Ofengemüse
## Menge:
2 Stück für 2 Personen
## Zutaten:
-2 Süßkartoffeln 
-200ml Tomatensauce
-1 Knoblauchzehe
-1 Dose Kidneybohnen
-2 Paprikas
-1 rote Zwiebel
-ein paar Kirschtomaten
-1 Chili
-veganer Reibekäse
- Paprikapulver
- Chilipulver
- Oregano
- Basilikum
## Zubereitung
- Ofen auf 200° vorheizen
- Süßkartoffeln waschen, längs halbieren und mit einer Gabel mehrmals einstechen. 
- Mit Öl, Salz und Pfeffer würzen und mit der Schnittseite nach für 15-20 Minuten in den Backofen tun.
- Die Hälfte der Zwiebel in Ringe und Paprika in Streifen schneiden. Zusammen mit den Tomaten in Öl, Salz, Pfeffer, Paprikapulver und den Chiliflocken marinieren.
- Zu den Süßkartoffeln geben und nochmal 15 Minuten backen.
- Knoblauch und Chili hacken, den Rest der Zwiebel grob würfeln.
- Öl in einer Pfanne erhitzen, Zwiebelwürfel darin ca. 4 Minuten anbraten. Dann Knoblauch und Chili dazugeben.
- Die Tomatensauce dazugeben und mit Salz, Pfeffer, Paprikapulver, Oregano und Basilikum würzen.
- ca 10 Minuten köcheln lassen, danach die Kidneybohnen hinzugeben.
- Wenn die Süßkartoffeln fertig weich sind, diese umdrehen, Käse darüber verteilen und das Innere mit einer Gabel zerdrücken. Die Sauce darüber geben und mit dem Ofengemüse anrichten.
- Fertig
