# Gefüllte Auberginen
## Menge:

2 Stück, für zwei Personen


## Zutaten:
### Auberginen:

- 2 Auberginen
- 1 Zwiebel
- 1 Knoblauchzehe
- Salz, Pfeffer, Paprika (edelsüß), Kurkuma
- 4 Tomaten
- 100g rote Linsen
- 500ml Wasser
- 100g Bulgur
- 1 Blatt glatte Petersilie
- etwas veganen Feta


### Sauce:

- 1 EL Öl
- 1 EL Tomatenmark
- etwas Wasser
- Salz


## Zubereitung:
### Auberginen:

- Auberginen waschen, halbieren, aushöhlen (bis auf 1 cm)
- Fruchtfleisch der Auberginen hacken
- Zwiebel und Knoblauch in 1 EL Öl anschwitzen
- Auberginenwürfel dazugeben
- nach 3-4 Minuten mit Salz, Pfeffer, Paprika (edelsüß) und Kurkuma würzen
- die Tomaten hacken und dazugeben
- Linsen und 300ml Wasser dazugeben und ca. 5 Minuten köcheln lassen
- Bulgur und 200ml Wasser dazugeben und weitere 5-7 Minuten köcheln lassen
- Petersilie hacken und dazugeben
- Auberginen befüllen
- Ofen auf 200°C (Ober-/Unterhitze) vorheizen
- Auberginen in Auflaufform geben
- etwas Feta darüberbröseln
- ca. 30 Minuten im Ofen backen


### Sauce:

- Zutaten mischen
- erhitzen
