# BBQ "Honey" Tofu
## Menge:

Beilage für 1-2 Personen


## Zutaten:

- 200g Tofu
- etwas Stärke
- ca. 3-4 EL BBQ Sauce
- 1-2 EL Agavendicksaft
- Öl


## Zubereitung:

- Tofu in ein Geschirrtuch einwickeln, beschweren und am besten ein paar Stunden liegen lassen.
- Danach in mundgerechte Dreiecke schneiden.
- Stücke in der Stärke wälzen.
- Tofu im heißen Öl knusprig braten und aus der Pfanne nehmen.
- Den Agavendicksaft und die BBQ Sauce zum restlichen Öl geben und so lange rühren,
  bis sie dickflüssig ist und überall Blasen sind.
- Den Tofu dazugeben und gut vermixen.
