# Tofu Burger
## Menge:
4 Stück für zwei Personen
## Zutaten:
- 400g Tofu
- 1 Zwiebel
- 2 EL Tomatenmark
- 1 EL Sojasauce
- 1 TL Paprikapulver
- Salz & Pfeffer
- 50g Mehl
## Zubereitung:
- Zwiebel klein hacken
- alle Zutaten vermischen
- Patties formen
- bei 175 Grad für 30 min in den Ofen, nach der Hälfte wenden
