# Gebratener Reis
## Menge:

2 Portionen


## Zutaten:

- 1 Tasse Reis
- Öl
- 200g Tofu
- Paprikapulver
- 2 Zehen Knoblauch, gepresst
- 2 Karotten
- ein paar grüne Bohnen (TK)
- ca o,5 Tassen Erbsen (TK)
- Salz, Chili, Koriander


### Sauce:

- 2 TL Agavendicksaft/Ahornsirup
- 3 EL Sojasauce
- 1 EL Sesamöl


## Zubereitung:

- Tofu in Geschirrtuch wickeln, beschweren und am besten ein paar Stunden liegen lassen
- Reis, Erbsen und Bohnen kochen
- gepressten Tofu in 1,5cm Würfel schneiden
- Tofu im Öl goldbraun braten, dann kräftig mit Sojasauce und Paprikapulver würzen
- Karotten in Scheiben schneiden
- Das ganze Gemüse in die Pfanne geben und anrösten
- Reis zugeben und mitbraten
- Zutaten für die Sauce vermischen und über den Reis geben
