# Kartoffel-Spinat-Gratin 
## Menge:

Für drei Personen


## Zutaten:

- 120g Cashewkerne
- 350ml Sojamilch
- 3 EL Haferflocken
- 2 EL Tapiokastärke
- 100ml Sojasahne
- 1 EL Muskat
- 1 Knoblauchzehe
- 450g Spinat
- 830g Kartoffeln
- Öl


## Vorbereitung:

- 80g der Cashewkerne über Nacht in kaltem Wasser einweichen lassen (mindestens 3 Stunden)
- den Spinat über Nacht im Kühlschrank auftauen lassen


## Zubereitung:

- die eingeweichten Cashewkerne abgießen und in den Mixer geben
- 300ml der Sojamilch und die Haferflocken dazu geben
- alles auf höchster Stufe zu einer cremigen Sauce pührieren
- Sauce mit der Tapiokastärke in einem Topf unter Rühren erhitzen, bis sie eingedickt ist
- die restlichen 50ml Sojamilch und die Sojasahne dazu geben
- mit dem Muskat, dem Knoblauch und reichlich Salz würzen
- Spinat gut ausdrücken
- Kartoffeln schälen und in dünne Scheiben hobeln
- Auflaufform mit Öl auspinseln
- Kartoffeln und Spinat im Wechsel in der Auflaufform aufschichten, dazwischen jeweils Sauce verteilen
- die restlichen Cashewkerne grob hacken und auf dem Gratin verteilen
- im vorgeheizten Backofen bei 180°C (Umluft) 30-40 Minuten backen
