# Mediterrane Nudelpfanne
## Menge: 
4 Portionen
## Zutaten:
- 500g Nudeln
- Salz
- 1 Zwiebel
- 3 Knoblauchzehen
- 200g getrocknete Tomaten in Öl
- 200g veganen Feta
- (passierte Tomaten) optional
- Basilikum nach Belieben
- Chiliflocken
- Pfeffer
# Zubereitung:
- Nudeln in Salzwasser kochen 
- Zwiebeln würfeln, Knoblauch fein hacken
- Tomaten und Feta in Würfel schneiden
- Tomaten mit Zwiebeln, Knoblauch und Chili anbraten, dann Feta, die passierten Tomaten und Basilikum dazugeben
- Mit Salz und Pfeffer abschmecken
- Nudeln abgießen und mit in die Pfanne geben
