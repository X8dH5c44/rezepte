# Soljanka
## Menge:
Für drei Personen
## Zutaten:
- 175g Räuchertofu
- 2 große Zwiebeln
- 350g Gewürzgurken (Abtropfgewicht)
- 150g Tomaten
- 680g Letscho (1 Glas)
- 3 EL Ketchup
- 2 TL Paprikapulver, edelsüß
- Salz, Pfeffer
- 3 EL Olivenöl
- 150ml Wasser
## Zubereitung:
- Den Räuchertofu in kleine Streifen schneiden (ca. 5mm x 5mm x 30mm)
- Im Topf das Olivenöl erhitzen und den Tofu hineingeben
- Die Zwiebeln in kleine Würfel schneiden und ebenfalls dazugeben
- Die Gewürzgurken in kleine Würfel schneiden
- Die Tomaten in mundgerechte Stücke schneiden
- Wenn der Tofu goldbraun ist,
  die Gewürzgurken und die Tomaten in den Topf geben
- Umrühren, sodass sich der angebackene Tofu vom Topfboden löst
- Das Einlegewasser der Gewürzgurken in den Topf gießen.
  Dabei mit dem Deckel oder einem Sieb verhindern,
  dass die Pfeffer- und Senfkörner in den Topf gelangen.
- Letscho in den Topf gießen
- Letscho-Glas mit 150ml Wasser füllen und die hängengebliebenen Reste mit in den Topf gießen
- Mit Paprikapulver, Salz und Pfeffer würzen
- Das Ketchup hinzugeben
- Umrühren und 20 Minuten köcheln lassen
