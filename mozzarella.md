# Mozzarella
## Menge:
1 Stück, etwa faustgroß
## Zutaten:
- 100g Cashewkerne
- 600ml Wasser
- 1 EL Hefeflocken
- 1 1/2 TL Apfelessig
- 2 TL Salz
- 1/4 TL Knoblauch(pulver)
- 3 EL Tapiokastärke
## Zubereitung:
- die Cashewkerne mit 200ml Wasser im Mixer fein mahlen
- Hefeflocken, Apfelessig, 1 TL Salz, Knoblauch und die Tapiokastärke hineinrühren
- bei starker Hitze in beschichtetem Topf inter ständigem Rühren erhitzen,
  bis sich eine glatte Kugel vom Topfboden löst
- 400ml Wasser und 1 TL Salz zu einer Sole verrühren
- von der Masse mit feuchtem Teelöffel Kugeln abstechen und in der Sole stocken lassen
## Hinweis:
Sofort verwenden oder bis zu 3 Tagen in der Sole im Kühlschrank lagern
