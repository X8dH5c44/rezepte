# Panierte Tofu-Sticks
## Menge:
Beilage für 1 bis 2 Personen
## Zutaten:
- 200g Tofu
### Marinade:
- 150ml Pflanzenmilch mit 1 EL Apfelessig
- 1TL Salz
- etwas Pfeffer
- 1 TL Paprikapulver
### Stärke:
- 30g Stärke
- 0,5 TL Salz
- 1 TL Paprikapulver
- 1 TL Knoblauchpulver
- 0,5 TL Thymian
- 0,5 TL Chilipulver
### Panade
- 50g Cornflakes
## Zubereitung:
- Den Tofu in ein Geschirrtuch einwickeln, beschweren und am besten ein paar Stunden stehen lassen
- alle Zutaten der Marinade mischen
- Tofu in ca 12 Stücke schneiden und für eine halbe bis 2 Stunden in die Marinade geben
- Zutaten für die Stärke mischen und Cornflakes im Mixer zerkleinern
- ein Tofustück in die Stärke, zurück in die Milch, dann in die Cornflakes geben und auf ein Backblech legen, mit allen Stücken wiederholen
- bei 180° Umluft für 20-25 min backen
