# Eier ersetzen beim Backen

**Hierbei handelt es sich um verschiedene Varianten und keinesfalls um die Zutaten für ein Rezept!**


## Kuchen, Pfannkuchen, Bratlinge:

1 Ei -> 
- 1/2 Banane
- 3 EL Apfelmus
- 1 EL Leinsamen mit 3 EL Wasser
- 3-4 EL zarte Haferflocken (Teig 15 Minuten quellen lassen)
- 1 EL Chiasamen und 3 EL Wasser (15 Minuten quellen lassen)
- 1 EL Sojajoghurt
- 1 EL Nussmus


## Burgerpatties o.ä.:

- 1-2 EL Tomatenmark
