# Brötchen "über Nacht"
## Menge:

8 Stück


## Zutaten:

- 500g Weizenmehl Typ 505
- 2 TL Salz
- 4g Frischhefe (alternativ 1 Messerspitze Trockenhefe)
- 320g Wasser (abwiegen!)
- 20g Öl


## Vorbereitung:

- Hefe in etwas Wasser auflösen
- mit restlichen Zutaten verkneten, bis ein glatter Teig entsteht,
  der nicht mehr klebt (mindestens 10 Minuten)
- gut verschlossen bei ca. 18°C über Nacht gehen lassen


## Zubereitung:

- morgens Teig etwas länglich ausziehen,
  die kurzen Enden zur Mitten falten,
  um 90° drehen und wiederholen
- Rolle formen, 8 Teigstücke à ca. 100g abstechen, jeweils flachdrücken
- Ränder zur Mitte einschlagen, damit Oberflächenspannung entsteht
  und mit der glatten Seite nach oben auf der Unterlage "schleifen"
- 15 Minuten auf Backblech gehen lassen
- mit scharfem Messer einschneiden
- Oberfläche mit Wasser besprühen oder bepinseln
- im Ofen bei 240°C 10 Minuten lang backen,
  danach weiter 10 Minuten bei mindestens 210°C
