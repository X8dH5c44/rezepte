# Gefüllte Süßkartoffeln
## Menge:

2 Portionen


## Zutaten:

- 2-3 Süßkartoffeln
- ein halbe Dose schwarze Bohnen (oder gekochte schwarze Linsen)
- eine halbe Dose Mais
- 2-3 EL Salsa Sauce
- Salz und Pfeffer
- Chilipulver
- veganer Käse, gerieben (Momentaner Favourit: Käseblock von Bedda)


## Zubereitung:

- Süßkartoffeln halbieren und für ca. 10 Minuten in die Mikrowelle geben,
  bis sie weich sind (Geht auch im Ofen, dauert aber viel länger)
- Das Innere der Süßkartoffeln auslöffeln, so dass ein 1cm breiter Rand bleibt.
- Das Süßkartoffelfleisch mit den restlichen Zutaten vermischen und zurück in die Kartoffeln geben.
- Mit Käse bestreuen.
- So lange in die Mikrowelle/Ofen geben, bis der Käse geschmolzen ist.
