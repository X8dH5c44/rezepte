# Marinaden für Tofu
**Da Tofu relativ geschmacksneutral ist, sollte er mariniert werden. Dafür werden hier verschiedene Varianten gesammelt.**
# Allgemein
Der Tofu sollte vorher in ein Geschirrtuch eingewickelt, beschwert und dann am besten ein paar Stunden liegen gelassen werden, damit die Flüssigkeit ausgepresst wird und die Marinade besser einziehen kann. Danach wird er in Würfel geschnitten und mindestens eine halbe Stunde in die Marinade gegeben, je länger desto besser. Dann kann er mit etwas Öl in der Pfanne gebraten werden.

## Asiatisch
- 5 EL Sojasauce
- 1 Knoblauchzehe
- 1 Chilischote
- Koriander
- Pfeffer
## Erdnuss
- 2 EL Sojasauce
- 1 Knoblauchzehe
- 2 EL Erdnussmus
- 1 EL Agavendicksaft
- Chilipulver
- Ingwer
## Mediterran
- 5 EL Olivenöl
- 1 EL Zitronensaft
- 0,5 Zwiebel
- 1 EL Basilikum
- Rosmarin
- Salz und Pfeffer
## Zitronen-Kräuter 
- Saft von 2 Zitronen
- 2 EL Agavendicksaft
- 2 EL italienische Gewürze
- 1 EL Hefeflocken
- 1 Knoblauchzehe
- Salz und Pfeffer
## Tessa Art 
- 1 Knoblauchzehe
- 2 EL brauner Zucker
- 1 EL Ketchup
- 1 EL Tomatenmark
- 2 EL Worcestersauce (oder Sojasauce)
- 1 TL Sriracha
- 1 EL Apfelessig
