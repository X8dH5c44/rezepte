# Hirseauflauf mit Broccoli und Feta
## Menge:

Für zwei Personen


## Zutaten:

- 1 Knoblauchzehe
- 2 mittelgroße Zwiebeln
- 20g Margarine
- 200g Hirse
- 500ml Gemüsebrühe
- 500g Broccoli
- 150g veganer Feta
- Salz, Pfeffer, Muskat
- 2 EL Schnittlauch
- 2 Tomaten


## Zubereitung:

- Knoblauch und Zwiebeln fein hacken
- beide in der Margarine andünsten
- Hirse heiß abwaschen und dazu geben
- mit der Gemüsebrühe auffüllen und 20-30 Minuten garen
- Broccoli 3-5 Minuten blanchieren
- veganen Feta und Tomaten würfeln
- Feta, Schnittlauch, Tomaten und Gewürze mit dem Rest vermengen
- im Backofen bei 180°-200°C kürz überbacken
