# Gnocchi
## Menge:

Für vier Personen


## Zutaten:

- 1kg Kartoffeln
- 350g Mehl
- 2EL Olivenöl
- je 1 Prise Salz, Pfeffer, Muskat


## Zubereitung:

- Kartoffeln schälen, 20-25 Minuten in Salzwasser garen
- Kartoffeln abgießen und 5-10 Minuten offen auf der abgeschalteten Herdplatte abdampfen lassen
- Kartoffeln fein zerstampfen
- Mehl und Gewürze unterheben
- pro Gnocchi etwa einen gestrichenen Esslöffel des Teigs nehmen und zu einer Kugel rollen
- Gnocchi, wie [hier](https://www.thecuriouschickpea.com/wp-content/uploads/2018/02/shapinggnocchitrio.jpg.webp)
  zu sehen über eine Gabel rollen, um ihnen die charakteristische Form zu verleihen
- Gnocchi in einen Topf mit kochenden Salzwasser geben (gegebenenfalls Etappenweise, bei größeren Mengen)
- Gnocchi eine Minute, nachdem sie an die Oberfläche treiben aus dem Topf nehmen
