# Nudeln mit Räuchertofu
## Menge:

2 Portionen; 2 Personen / 1 sehr hungrige Person


## Zutaten:

- 250g Nudeln (Penne, Fusili...)
- 187g Räuchertofu (ein Block aus einem handelsüblichen Doppelpack)
- 1 Tomate (faustgroß)
- Pfeffer
- geräucherte Paprika (Gewürz)
- Sojasauce
- Olivenöl


## Vorbereitung:

- Räuchertofu in Streifen schneiden (5mm x 5mm x 5cm)
- Tomate in mundgerechte Stücke schneiden


## Zubereitung:

- Wasser für Nudeln zum Kochen bringen
- Nudeln in den Topf tun
- Wecker auf 10 Minuten stellen
- Öl in die Pfanne geben und erhitzen
- Tofu in die Pfanne geben
- Etwas anbraten lassen
- Mit Pfeffer würzen
- KEIN SALZ HINZUFÜGEN
  - Die Sojasauce hat genug davon
- Nach einiger Zeit mit geräucherter Paprika nachwürzen
- Wenn die Nudeln fertig sind diese zusammen mit den Tomaten ebenfalls in die Pfanne geben
- Sojasauce hinzugeben (so, dass in etwa alles etwas davon abbekommt)
- 1 bis 2 Minuten warten
- Spaß haben
