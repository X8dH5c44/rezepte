# Kichererbsen Curry
## Menge: 
2 Portionen 
## Zutaten: 
- 1 Zwiebel
- 2 Knoblauchzehen
- ein ca 2cm Ingwer Stück
- eine halbe Chili, oder Chilipulver
- 1 EL Curry
- 1 TL Kümmel
- 1 TL Koriander
- 1 TL Paprikapulver
- 1 Dose (400g) gehackte Tomaten
- 0,5-1 Dose Kokosmilch
- 1 Dose Kichererbsen
- 100-150g frischen Spinat
- etwas Olivenöl
- Salz und Pfeffer
- Saft einer halben Limette

## Dazu passt:
- Reis oder Naan
- Tahini 
## Zubereitung:
- Zwiebel, Knoblauch und Chili schneiden und im Öl glasig braten.
- Die Gewürze hinzugeben und etwas mitbraten
- Tomaten, Spinat, Kichererbsen und Kokosmilch hinzugeben und solange köcheln bis der Spinat fertig ist.
- Mit Salz, Pfeffer und Limettensaft würzen.
