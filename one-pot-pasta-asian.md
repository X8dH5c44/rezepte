# One Pot Pasta
## Menge:
Für zwei Personen
## Zutaten:
- 1 Zwiebel, fein gehackt
- 2 Knoblauchzehen, fein gehackt
- 1 Zucchini, gewürfelt
- 1 rote Paprika
- 80g gefrorene Erbsen
- 250g Fusili
- 1 1/2 TL vegane rote Chilipaste
- 250ml Kokosmilch
- 400g stückige Tomaten (aus der Dose oder so ner "Tüte?")
- 1 Hand voll Cherrytomaten, halbiert
- 1TL Zitronensaft
- Salz
- Pfeffer
## Zubereitung:
- Zwiebel 2-3 Minuten glasig braten
- Knoblauch, Zucchini und Paprika dazu geben, weitere 2 Minuten braten
- Erbsen, Fusili, Chilipaste, Kokosmilch und Tomaten aus der Dose zufügen,
- ca. 15 Minuten bei mittlerer Hitze kochen
- Cherrytomaten 2 Minuten vor Ende der Garzeit zugeben
- Zitronensaft, Salz und Pfeffer nach Gefühl abschmecken
